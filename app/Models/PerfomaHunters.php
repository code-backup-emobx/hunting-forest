<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PerfomaHunters extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'perfoma_of_hunters';
}