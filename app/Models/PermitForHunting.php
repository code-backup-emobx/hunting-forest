<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PermitForHunting extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'permit_for_huntings';
}