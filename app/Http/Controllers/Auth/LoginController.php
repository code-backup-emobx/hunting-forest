<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Session;
use Validator;
use App\Models\AllUsers;


class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    
   
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showlogin(){
        return view('auth.login');
    }

    public function login(Request $request){

        $validator = Validator::make($request->all(), [
                    
                'phone_number' => 'required'
                ]);

            if ($validator->fails()) 
            {
                return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
            }
        $phone_number = $request->phone_number;
         
        if (AllUsers::where('phone_number',$phone_number)->where('status','1')->first()) {
            return redirect('/otp');
        }
        else{
            return redirect()->back()->with('fail','!Enter the valid phone_number');
        }
    }

    //Otp
    public function otp(){

        return view('auth.otp');
    }

    public function otp_login(Request $request){

        $validator = Validator::make($request->all(), [
                    
                'otp' => 'required'
                ]);

            if ($validator->fails()) 
            {
                return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
            }
        $otp = $request->otp;
        
        if (AllUsers::where('otp',$otp)->where('status','1')->first()) {
            return redirect('/permit_for_hunting');
        }
        else{
            return redirect()->back()->with('fail','!Enter the valid otp');
        }        
    }
 
}

?>