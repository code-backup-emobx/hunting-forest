<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use App\Models\AllUsers;
use App\Models\PermitForHunting;
use App\Models\PerfomaHunters;


class HomeController extends Controller
{

	// public function __construct()
 //    {
 //    	$this->middleware('auth');
 //    }

	public function index(){
		
		return view('/permit_for_hunting');
	}
	public function submitPermatHunting(Request $request){



		$validator = Validator::make($request->all(), [
                    
            'profile_pic' => 'required',
            'office_of_the' => 'required',
            'permit_number' => 'required',
            'date_of_issue' => 'required',
            'name_address_of_applicant' => 'required',
            'permit_type'  => 'required',
            'applied_permit_area' => 'required',
            'name_of_hunter'     => 'required',
            'arms_licence_no_its_validity' => 'required',
            'rifle_no_used_hunting' => 'required',
            'weapon_description' => 'required',
            'species_of_animal' => 'required',
            'no_of_animal_hunted' => 'required'
        ]);

            if ($validator->fails()) 
            {
                return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
            }

		$profile_pic = $request->input('profile_pic');
		$office_of_the = $request->input('office_of_the');
		$permit_number = $request->input('permit_number');
		$date_of_issue = $request->input('date_of_issue');
		$name_address_of_applicant = $request->input('name_address_of_applicant');
		$permit_type = $request->permit_type;
		$applied_permit_area = $request->input('applied_permit_area');
		$name_of_hunter = $request->input('name_of_hunter');
		$arms_licence_no_its_validity = $request->input('arms_licence_no_its_validity');
		$rifle_no_used_hunting = $request->input('rifle_no_used_hunting');
		$weapon_description = $request->input('weapon_description');
		$species_of_animal = $request->species_of_animal;
		$no_of_animal_hunted = $request->input('no_of_animal_hunted');

		 if($request->hasFile('profile_pic') != "") {
            $profile_pic = $request->file('profile_pic');
            // $filename = time() . '.' . $profile_pic->getClientOriginalExtension();
            $filename =$profile_pic->getClientOriginalName();
            $destinationPath = public_path('/media');
            $profile_pic->move($destinationPath, $filename);
            $profile_pic = '/media/' . $filename;

			$submit_permit = new PermitForHunting();
			$submit_permit->profile_pic = $profile_pic;
			$submit_permit->office_of_the = $office_of_the;
			$submit_permit->permit_number = $permit_number;
			$submit_permit->date_of_issue = $date_of_issue;
			$submit_permit->name_address_of_applicant = $name_address_of_applicant;
			$submit_permit->permit_type = $permit_type;
			$submit_permit->applied_permit_area = $applied_permit_area;
			$submit_permit->name_of_hunter = $name_of_hunter;
			$submit_permit->arms_licence_no_its_validity = $arms_licence_no_its_validity;
			$submit_permit->rifle_no_used_hunting = $rifle_no_used_hunting;
			$submit_permit->weapon_description = $weapon_description;
			$submit_permit->species_of_animal = $species_of_animal;
			$submit_permit->no_of_animal_hunted = $no_of_animal_hunted;
			$submit_permit->save();
			return back()->with('success','Successfully added');
		}
	}

	public function perfomaForHunter(){

		return view('/perfoma_for_hunter');
	}

	public function submitPerfomaForHunter(Request $request){

		$name = $request->input('name');
		$father_name = $request->input('father_name');
		$age = $request->input('age');
		$permanent_address = $request->input('permanent_address');
		$arm_license_no = $request->input('arm_license_no');
		$arm_license_validity = $request->input('arm_license_validity');
		$rifle_number =	$request->input('rifle_number');
		$weapon_description = $request->input('weapon_description');

		$submit_perfoma = new PerfomaHunters();
		$submit_perfoma->name = $name;
		$submit_perfoma->father_name = $father_name;
		$submit_perfoma->age = $age;
		$submit_perfoma->permanent_address = $permanent_address;
		$submit_perfoma->arm_license_no = $arm_license_no;
		$submit_perfoma->arm_license_validity = $arm_license_validity;
		$submit_perfoma->rifle_number = $rifle_number;
		$submit_perfoma->weapon_description = $weapon_description;
		$submit_perfoma->save();

		return back()->with('success','Successfully added perfoma');

	}
// end class 
}