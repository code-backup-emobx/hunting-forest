@extends('layouts.app1')

@section('content')

  <!-- <div class="d-flex flex-column-fluid"> -->
        <!--begin::Container-->
        <div class=" container ">
            <div class="card card-custom">
    <div class="card-body p-0">
        <!--begin: Wizard-->
        <div class="wizard wizard-2" id="kt_wizard_v2" data-wizard-state="step-first" data-wizard-clickable="false">
            <!--begin: Wizard Nav-->
            <div class="wizard-nav border-right py-8 px-8 py-lg-20 px-lg-10" style="background-color: #297430">
           
                <!--begin::Wizard Step 1 Nav-->
                <div class="wizard-steps" >
                  
                             <!--begin::Wizard Step 1 Nav-->
               
                   
                    <div class="wizard-step" data-wizard-type="step" data-wizard-state=""style="background-color: #048404">
                        <div class="wizard-wrapper">
                            <div class="wizard-icon">
                                <span class="svg-icon svg-icon-2x"><!--begin::Svg Icon | path:assets/media/svg/icons/General/User.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
        <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero"/>
    </g>
</svg><!--end::Svg Icon--></span>                            </div>
                            <div class="wizard-label">
                                <h3 class="wizard-title text-white">
                                      Certificate Verification 
                                </h3>
                                <div class="wizard-desc text-white">
                                 
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Wizard Step 1 Nav-->

                   

                 

                


                     

                   
                </div>
            </div>
            <!--end: Wizard Nav-->

            <!--begin: Wizard Body-->
            <div class="wizard-body py-8 px-8 py-lg-20 px-lg-10">
                <!--begin: Wizard Form-->
                <div class="row">
                    <div class="offset-xxl-2 col-xxl-8">
                        <form class="form" id="kt_form" action={{route('verify_certificate')}} method="post">
                            @csrf
                            <!--begin: Wizard Step 1-->
                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                 <h4 class="mb-10" style='color:green'>Cerificate Verification With Application Number Or Reference Number</h4>
                                  <!--begin::Input-->
                                        <div class="form-group">
                                            <label><b>Application Number</b></label>
                                            <input type="text" class="form-control form-control-lg" name="application_no" id="application_no" placeholder="Application number " value="{{old('application_no')}}" onkeypress="onlyNum(event);"  />
                                            <span class="form-text text-muted" >Please enter application number.</span>
                                        </div>
                                        <!--end::Input-->

                                        <div class="form-group">
                                            <center>OR</center>
                                         
                                        </div>
                                        
                                        <!--begin::Input-->
                                        <div class="form-group">
                                            <label><b> Reference Number </b></label>
                                            <input type="text" class="form-control form-control-lg" name="ref_no" id="ref_no" placeholder="Reference number " value="{{old('ref_no')}}" onkeypress="onlyNum(event);"  />
                                            <span class="form-text text-muted" >Please enter Reference number.</span>
                                        </div>
                                        <!--end::Input-->
                                  
                                    
                                        <!--begin::Input-->

                                         <div class="form-group">
                                           <label><b id="mobile_status" class="text-danger">&nbsp;</b></label>
                                            <button type="submit" id="login_btn"class="form-control btn btn-light-success" />
                                            Verify
                                           </button>
                                    
                                        </div>
                                        <!--end::Input-->
                                  

                                 
                                 
                                   
                                
                               
                                

                               
                                    </div>

                                    
                                   
                                </div>







                 </div>
                            <!--end: Wizard Step 1-->
                          
                           
                           

                            <!--begin: Wizard Actions-->
                            <div class="d-flex justify-content-between border-top mt-5 pt-10">
                                <div class="mr-2">
                                    <!-- <button type="button" class="btn btn-light-primary font-weight-bold text-uppercase px-9 py-4">
                                        Previous
                                    </button> -->
                                </div>
                                <div>
                                    <!-- <button type="button" class="btn btn-success font-weight-bold text-uppercase px-9 py-4" >
                                        Submit
                                    </button> -->
                                  <!--   <button type="submit" class="btn btn-primary font-weight-bold text-uppercase px-9 py-4" >
                                  
                                    </button> -->
                                </div>
                            </div>
                            <!--end: Wizard Actions-->
                        </form>
                    </div>
                    <!--end: Wizard-->
                </div>
                <!--end: Wizard Form-->
            </div>
            <!--end: Wizard Body-->
        </div>
        <!--end: Wizard-->
    </div>
</div>
        </div>
        <!--end::Container-->
<!--     </div> -->

@endsection
@push('page-script')


<script>
  function verify_certificate(){
    
             var application_no=$('#application_no').val();
             var ref_no=$('#ref_no').val();
          

              

              if(ref_no=='' &&  application_no==''){
                    Swal.fire("Fill atleast one field!","", "warning");
                   return false;
               }     
                   $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            },
                            type: "POST",
                            url: "/verify_certificate",
                            data: {application_no:application_no,ref_no:ref_no},
                            success: function(result){
                           
                                if(result.error_code == '200'){
                                 
                                    Swal.fire(result.message,"", "success");
                                }
                                else{
                                     Swal.fire(result.message,"", "error");

                                }
                            }
                                
                    });
    } 
</script>  







<script>
  function check_otp(application_no_id,otp_id,enter_otp_field,click_verify_field,after_verify_field){

             var otp=$('#'+otp_id).val();
             var application_no=$('#'+application_no_id).val();
             if(otp.length != '6'){
                    Swal.fire("Invalid","", "error");
                   return false;
             }

              if(!$.isNumeric(otp)){
                   Swal.fire("Invalid","", "error");
                   return false;
             } 
             //otp=otp*8448346861; 
                   $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            },
                            type: "POST",
                            url: "check_otp_by_application_number",
                            data: {otp:otp,application_no:application_no},
                            success: function(result){
                            console.log(result);
                                if(result.error_code == '200'){
                                Swal.fire(result.message,"", "success");
                                $('#'+enter_otp_field).prop('hidden',true);
                                $('#'+click_verify_field).prop('hidden',true);
                                $('#'+after_verify_field).prop('hidden',false);
                                $('#mobile_verify').val('1');
                                window.location.reload();
                              
                               

                                }
                                else{
                                Swal.fire(result.message,"", "error");
                                }
                            }
                                
                    });
    } 
</script> 
 




@endpush