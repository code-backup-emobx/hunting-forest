@extends('layouts.app1')

@section('content')

  <!-- <div class="d-flex flex-column-fluid"> -->
        <!--begin::Container-->

    <div class=" container ">
            <div class="card card-custom">
    <div class="card-body p-0">
        <!--begin: Wizard-->
        <div class="wizard wizard-2" id="kt_wizard_v2" data-wizard-state="step-first" data-wizard-clickable="false">
            <!--begin: Wizard Nav-->
            <div class="wizard-nav border-right py-8 px-8 py-lg-20 px-lg-10" style="background-color: #297430">
                <!--begin::Wizard Step 1 Nav-->
             
                <div class="wizard-steps" >

                    <!--end::Wizard Step 1 Nav-->
                    <div class="wizard-step" data-wizard-type="step" data-wizard-state=""style="background-color: #048404">
                        <div class="wizard-wrapper">
                            <div class="wizard-icon">
                                <span class="svg-icon svg-icon-2x"><!--begin::Svg Icon | path:assets/media/svg/icons/General/User.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							        <polygon points="0 0 24 0 24 24 0 24"/>
							        <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
							        <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero"/>
							    </g>
								</svg><!--end::Svg Icon--></span>                            
							</div>
                            <div class="wizard-label">
                                <h3 class="wizard-title text-white">
                                     <a href="{{url('/permit_for_hunting')}}" style="color:white;">Permit For Hunting</a>
                                </h3>
                                <!-- <div class="wizard-desc text-white">
                                  
                                </div> -->
                            </div>

                        </div>
                    </div>
                    <div style="padding:2%;"></div>
                    <!-- Wizard second start -->
                     <div class="wizard-step" data-wizard-type="step" data-wizard-state=""style="background-color: #048404">
                        <div class="wizard-wrapper">
                            <div class="wizard-icon">
                                <span class="svg-icon svg-icon-2x"><!--begin::Svg Icon | path:assets/media/svg/icons/General/User.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon points="0 0 24 0 24 24 0 24"/>
                                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero"/>
                                </g>
                                </svg><!--end::Svg Icon--></span>                            
                            </div>
                            <div class="wizard-label">
                                <h3 class="wizard-title text-white">
                                     <a href="{{url('/perfoma_for_hunter')}}" style="color:white;">Perfoma For Hunting</a>
                                </h3>
                                <!-- <div class="wizard-desc text-white">
                                  
                                </div> -->
                            </div>

                        </div>
                    </div>
                    <!-- Wizard second ends -->
                    <!--end::Wizard Step 1 Nav-->

                   

                    <!--begin::Wizard Step 2 Nav-->
                    <!-- <div class="wizard-step" data-wizard-type="step">
                        <div class="wizard-wrapper">
                            <div class="wizard-icon">
                                <span class="svg-icon svg-icon-2x"><!--begin::Svg Icon | path:assets/media/svg/icons/General/Thunder-move.svg--><!-- <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							        <rect x="0" y="0" width="24" height="24"/>
							        <path d="M16.3740377,19.9389434 L22.2226499,11.1660251 C22.4524142,10.8213786 22.3592838,10.3557266 22.0146373,10.1259623 C21.8914367,10.0438285 21.7466809,10 21.5986122,10 L17,10 L17,4.47708173 C17,4.06286817 16.6642136,3.72708173 16.25,3.72708173 C15.9992351,3.72708173 15.7650616,3.85240758 15.6259623,4.06105658 L9.7773501,12.8339749 C9.54758575,13.1786214 9.64071616,13.6442734 9.98536267,13.8740377 C10.1085633,13.9561715 10.2533191,14 10.4013878,14 L15,14 L15,19.5229183 C15,19.9371318 15.3357864,20.2729183 15.75,20.2729183 C16.0007649,20.2729183 16.2349384,20.1475924 16.3740377,19.9389434 Z" fill="#000000"/>
							        <path d="M4.5,5 L9.5,5 C10.3284271,5 11,5.67157288 11,6.5 C11,7.32842712 10.3284271,8 9.5,8 L4.5,8 C3.67157288,8 3,7.32842712 3,6.5 C3,5.67157288 3.67157288,5 4.5,5 Z M4.5,17 L9.5,17 C10.3284271,17 11,17.6715729 11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L4.5,20 C3.67157288,20 3,19.3284271 3,18.5 C3,17.6715729 3.67157288,17 4.5,17 Z M2.5,11 L6.5,11 C7.32842712,11 8,11.6715729 8,12.5 C8,13.3284271 7.32842712,14 6.5,14 L2.5,14 C1.67157288,14 1,13.3284271 1,12.5 C1,11.6715729 1.67157288,11 2.5,11 Z" fill="#000000" opacity="0.3"/>
							    </g>
								</svg><!--><!-- end::Svg Icon</span>  
							</div>  
                            <div class="wizard-label">
                                <h3 class="wizard-title text-white">
                                     Upload Ownership Documents
                                </h3>
                                <div class="wizard-desc text-white">
                                     Upload Your Ownership Documents
                                </div>
                            </div>
                        </div>
                    </div> 
                    <!--end::Wizard Step 2 Nav-->
                </div>
            </div>
            <!--end: Wizard Nav-->

            <!--begin: Wizard Body-->
            <div class="wizard-body py-8 px-8 py-lg-20 px-lg-10">
                <!--begin: Wizard Form-->
                <div class="row">
                    <div class="offset-xxl-2 col-xxl-8">
                        <form class="form" id="kt_form" action="{{url('/permit_for_hunting')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <!--begin: Wizard Step 1-->
                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                <h4 class="mb-5" style='color:green'>[Under Sectin 11(1)(b) of the Wildlife (Protection) Act,1972]</h4>
                                <h4 class="mb-10" style='color:green'>Enter your Details</h4>

                                <!--begin::Input-->
                                <div class="form-group">
                                    <label><b>Profile Picture </b><font color="red"> * </font></label>
                                    <input type="file" class="form-control form-control-lg" name="profile_pic" id="profile_pic" placeholder="Please upload the picture" value=""  />
                                    <span class="form-text text-muted">Please upload the picture.</span>
                                </div>

                                <div class="form-group">
                                    <label><b>Office of the </b><font color="red"> * </font></label>
                                    <input type="text" class="form-control form-control-lg" name="office_of_the" id="office_of_the" placeholder="Please enter office of the" value="{{old('office_of_the')}}"  />
                                    <span class="form-text text-muted">Please enter office of the.</span>
                                </div>
                                <!--end::Input-->
                             
                                <div class="row">
                                    <div class="col-xl-6">
                                		<div class="form-group">
                                           <label><b>Permit Number </b><font color="red"> * </font></label>
                                            <input type="text" class="form-control form-control-lg" name="permit_number"  id="permit_number" placeholder="Permit number" value="{{old('permit_number')}}"  />
                                            <span class="form-text text-muted">Please enter permit number.</span>
                                        </div>
                                    </div>
                                    <div class="col-xl-6">
	                                	<div class="form-group">
	                                        <label><b>Date Of issue</b><font color="red"> * </font></label>
	                                        <input type="text" class="form-control form-control-lg date" name="date_of_issue" id="datetimepicker1" data-provide="datepicker" placeholder="Enter issue date" value="{{old('date_of_issue')}}"  autocomplete="off" data-date-format="yyyy/mm/dd"/>
	                                        <span class="form-text text-muted">Please enter issue date.</span>
	                                    </div>
                                    </div>
                                </div> 
                               
                                <div class="form-group">
                                    <label><b>Name and address of the Applicant</b><font color="red"> * </font></label>
                                    <textarea  class="form-control form-control-lg" name="name_address_of_applicant" id="name_address_of_applicant">{{old('name_address_of_applicant')}}
                                    </textarea>
                                    <span class="form-text text-muted">Please enter name and address.</span>
                                </div>
                              

                                <div class="form-group">
                                    <label><b> Permit for </b><font color="red"> * </font></label>
                                    <select id="permit_type" name="permit_type" class="form-control selectpicker" onchange="myFunction()">
                                      <option value="">--Select Please--</option>
                                      <option value="crop_field">Crop Field</option>
                                      <option value="air_force"> Air Force</option>  
                                    </select>
                                   
                                  <span class="form-text text-muted">Please Choose Permit for.</span>
                                </div>  
                                <!--begin::Input-->

                                <div class="form-group">
                                    <label id="area_permit"><b>Name of Air Force Station for which permit is applied</b><font color="red"> * </font></label>
                                    <textarea  class="form-control form-control-lg" name="applied_permit_area"  id="applied_permit_area">{{ old('applied_permit_area') }}</textarea>
                                    <span class="form-text text-muted">Please enter area for which permit is applied.</span>
                                </div>
                              
                                <!--end::Input-->
                     
                                 <!--begin::Input-->
                                <div class="form-group">
                                    <label><b>Name of hunter </b><font color="red"> * </font></label>
                                    <input type="text" class="form-control  form-control-lg" name="name_of_hunter" id="name_of_hunter" placeholder="Name of hunter" value="{{ old('name_of_hunter') }}" />
                                    <span class="form-text text-muted">Please enter Name of hunter.</span>
                                </div>

                                <div class="form-group">
                                    <label><b>Hunter's Arms Licence No. and its validity </b><font color="red"> * </font></label>
                                    <input type="text" class="form-control  form-control-lg" name="arms_licence_no_its_validity" id="arms_licence_no_its_validity" placeholder="Hunter's Arms Licence No. and its validity" value="{{ old('arms_licence_no_its_validity') }}" />
                                    <span class="form-text text-muted">Please enter Hunter's Arms Licence No. and its validity.</span>
                                </div>

                                <div class="form-group">
                                    <label><b>Rifle No. to be used for Hunting</b><font color="red"> * </font></label>
                                    <input type="text" class="form-control  form-control-lg" name=" rifle_no_used_hunting" id=" rifle_no_used_hunting" placeholder="Rifle No. to be used for Hunting" value="{{old('rifle_no_used_hunting')}}" />
                                    <span class="form-text text-muted">Rifle No. to be used for Hunting.</span>
                                </div>

                                <div class="form-group">
                                    <label><b>Descriptin of weapon</b><font color="red"> * </font></label>
                                    <textarea  class="form-control form-control-lg" name="weapon_description" id="weapon_description">{{ old('weapon_description') }}</textarea>
                                    <span class="form-text text-muted">Please enter Descriptin of weapon.</span>
                                </div>

                                <div class="form-group">
                                  <h5 class="mb-4" style='color:green'>This permit is valid for hunting of following species:</h5>
                                    <label><b> Species of animal </b><font color="red"> * </font></label>
                                    <select id="species_of_animal" name="species_of_animal" class="form-control selectpicker">
                                      <option value="">--Select Please--</option>
                                      <option value="wild_boar">Wild Boar( Sus scrofa)</option>
                                      <option value="roz">Roz (Boselaphus tragocameolus)</option>  
                                    </select>
                                  <span class="form-text text-muted">Please Choose Species of animal.</span>
                                </div>

                                <div class="form-group">

                                    <label><b>Number of animals to be hunted</b><font color="red"> * </font></label>
                                    <input type="number" class="form-control  form-control-lg" name="no_of_animal_hunted" id="no_of_animal_hunted" placeholder="Rifle No. to be used for Hunting" value="{{old('no_of_animal_hunted')}}" />
                                    <span class="form-text text-muted">Number of animals to be hunted.</span>
                                </div>

                                 <div class="form-group">

                                  <h5 class="mb-4" style='color:green'>Conditions of the Permit:</h5>
                                  
                                </div>

                                <!--end::Input-->
                                <button type="submit" class="btn btn-sm font-weight-bold text-uppercase px-9 py-4" style="background-color:#297430;color:white;font-size: initial;" >Submit

                         
							       <!--end::Input-->
                       
                 		</div>
                            <!--end: Wizard Step 1-->
                          
                            <!--begin: Wizard Actions-->
                           
                            <!--end: Wizard Actions-->
                        </form>
                    </div>
                    <!--end: Wizard-->
                </div>
                <!--end: Wizard Form-->
            </div>
            <!--end: Wizard Body-->
        </div>
        <!--end: Wizard-->
    </div>
</div>
        </div>
        <!--end::Container-->
<!--     </div> -->

@endsection

<script>
function myFunction() {
  var x = document.getElementById("permit_type").value;
  if( x == 'crop_field'){
    document.getElementById("area_permit").innerHTML = '<label id="area_permit"><b>Area for which permit is applied</b><font color="red"> * </font></label>';
  }
  else{
        document.getElementById("area_permit").innerHTML = '<label id="area_permit"><b>Name of Air Force Station for which permit is applied</b><font color="red"> * </font></label>';
  }
}
</script>


    