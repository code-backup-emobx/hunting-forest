<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});



Route::get('login',[\App\Http\Controllers\Auth\LoginController::class,'showlogin'])->name('login');
Route::post('login',[\App\Http\Controllers\Auth\LoginController::class,'login'])->name('login');

Route::get('otp',[\App\Http\Controllers\Auth\LoginController::class,'otp'])->name('otp');
Route::post('otp',[\App\Http\Controllers\Auth\LoginController::class,'otp_login'])->name('otp');

Route::get('permit_for_hunting',[\App\Http\Controllers\HomeController::class,'index'])->name('permit_for_hunting');
Route::post('permit_for_hunting',[\App\Http\Controllers\HomeController::class,'submitPermatHunting'])->name('permit_for_hunting');

Route::get('perfoma_for_hunter',[\App\Http\Controllers\HomeController::class,'perfomaForHunter'])->name('perfoma_for_hunter');

Route::post('perfoma_for_hunter',[\App\Http\Controllers\HomeController::class,'submitPerfomaForHunter'])->name('perfoma_for_hunter');